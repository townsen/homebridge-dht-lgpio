# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

Nothing yet

## [0.9.3] - 2024-06-11

### Fixed

- Typo in plugin title

## [0.9.2] - 2024-06-11

### Added

- Nothing

### Fixed

- EACCESS error when running under virtiofs on MacOS
  (Spawn /usr/bin/env python3 instead of using shebang)

### Changed

- Nothing

### Removed

- Nothing

## [0.9.1] - 2024-05-22

First release with new plugin name
