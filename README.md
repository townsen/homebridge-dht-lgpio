# homebridge-dht-rgpio

A temperature/humidity sensor service plugin for [Homebridge](https://github.com/nfarina/homebridge) using a
[DHTxx](https://shop.pimoroni.com/products/cm2302-dht22-temperature-humidity-sensor-module?variant=39365437653075).
The sensor is accessed using the `python3-rgpio` package which means this works on kernels 5.11 and above (when the kernel GPIO interface changed).

* Display of temperature, humidity from a DHTxx connected to a RaspberryPI.
* Support the graphing feature of the Eve app for trends

## Installation

On the machine hosting HomeBridge:

1.	Install Homebridge using `npm install -g homebridge`
2.	Install Python3 rgpio with `sudo apt-get install python3-rgpio`
3.	Install this plugin `npm install -g @townsen/homebridge-dht-lgpio`
4.	Update your configuration file - see below for an example

On the machine to which the DHTxx is attached:

1. Install the rgpio daemon with `sudo apt-get install rgpiod`

## Configuration

* `accessory`: "DHTxx"
* `name`: descriptive name
* `loglevel`: 0 = none, 1 = invalid status codes, 2 = each reading, errors are always logged
* `hostname`: The name of the remote host
* `gpiochip`: the gpio chip to use (optional, default 0)
* `gpio`: the pin to which the device is connected (optional, default 4)
* `name_temperature` (optional): descriptive name for the temperature sensor
* `name_humidity` (optional): descriptive name for the humidity sensor
* `refresh`: Optional, time interval for refreshing data in seconds, defaults to 30 seconds.
* `recordpath`: Optional, used by the fakegato history code to create an on-disk record of
  the observations. This preserves the history in case of a restart, and can be used to
  extract the observations manually.  The sensor device is named `dht-<gpiochip>-<gpio>`
  which is then used to create the filename: `homebridge_<device>_persist.json`.

Simple Configuration

This is fine if you just have one device called "Temperature", but if you have more then
their persisted readings will collide!

```json
{
  "bridge": {
    "name": "Example for DHT22 using LGPIO",
    "username": "AB:A2:44:66:6E:36",
    "port": 51826,
    "pin": "023-66-204"
  },
  "accessories": [
    {
      "accessory": "DHTxx",
      "name": "Sensor",
      "name_temperature": "Temperature",
      "name_humidity": "Humidity",
      "recordpath": "/save/me",
      "refresh": 35,
      "hostname": "remote.machine",
      "gpiochip": 0
      "gpio": 5
    }
  ],
  "platforms": []
}
```

This plugin creates two services: TemperatureSensor and HumiditySensor.

## Credits

This was adapted from the two projects below:
* homebridge-bme280
* homebridge-am2320

The python script to read the DHT devices came from

* [The driver writer](http://abyz.me.uk/lg/py_rgpio.html)


## License

MIT
